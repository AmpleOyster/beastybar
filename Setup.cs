﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace BeastyBar
{
    public static class Setup
    {
        public static Dictionary<int, string> Colours = new Dictionary<int, string>
        {
            {0, "Red"},
            {1, "Blue"},
            {2, "Yellow"},
            {3, "Green"}            
        };

        public static List<Animal> JostlingArea = new List<Animal>();

        public static List<Animal> ThatsIt = new List<Animal>();

        public static List<Animal> Entered = new List<Animal>();

        public static List<Player> Players = new List<Player>();

        public static Dictionary<Type, string> LastPlayedCard = new Dictionary<Type, string>();

        public static bool EntranceLeft = true;

        public static void AddPlayers()
        {

            string username;
            int playerCount = 0;

            while (playerCount < 4)
            {    
                username = GetUsername(playerCount + 1);
                Player player = new Player(username, playerCount, Colours[playerCount]);
                player.DealCards();
                Players.Add(player);
                playerCount++;
            }
        }

        public static string GetUsername(int playerNum)
        {               
            string askForUser;
            string confirmUsername;
            string username = string.Empty;
            string usernameConfirmation;
            bool finished = false;
            bool rename = true;

            while (!finished)
            {
                
                if (rename)
                {
                    // Type your username and press enter
                    askForUser = $"Enter username for player {playerNum}: ";
                    Console.WriteLine(askForUser);
                    username = Console.ReadLine();
                    Console.WriteLine(Environment.NewLine);
                }
                
                if(username.TrimStart() != string.Empty)
                {
                    //Check username
                    confirmUsername = $"Confirm username: {username} click y to confirm n to rename [y/n]: ";
                    Console.WriteLine(confirmUsername);
                    usernameConfirmation = Console.ReadLine();
                    Console.WriteLine(Environment.NewLine);
                    if (usernameConfirmation.ToLower() == "y")
                    {
                        finished = true;
                    }
                    else if (usernameConfirmation.ToLower() == "n")
                    {
                        rename = true;
                    }
                    else
                    {
                        Console.WriteLine("Input not recognised, please try again");
                        Console.WriteLine(Environment.NewLine);
                        rename = false;
                    }
                }
                else
                {
                    Console.WriteLine("Input not recognised, please try again");
                    Console.WriteLine(Environment.NewLine);
                }
            }

            return username;
        }
        

        public static void ResolveRoundEnd()
        {           
            if (JostlingArea.Count == 5)
            {
               
                Animal acceptCardOne = JostlingArea[0];
                Animal acceptCardTwo = JostlingArea[1];
                Animal rejectCard = JostlingArea[JostlingArea.Count-1];

                //Add to entry
                Entered.Add(acceptCardOne);
                Entered.Add(acceptCardTwo);

                Console.WriteLine(Environment.NewLine);
                Console.WriteLine($" The {acceptCardOne.Colour} {acceptCardOne.Name} and the {acceptCardTwo.Colour} {acceptCardTwo.Name} have both entered the bar!");
                Console.WriteLine(Environment.NewLine);

                //Add to barred
                ThatsIt.Add(rejectCard);

                Console.WriteLine($" ...while the {rejectCard.Colour} {rejectCard.Name} was kicked out of the line by the beast bouncers!");
                Console.WriteLine(Environment.NewLine);

                //Remove card from jostling area
                JostlingArea.RemoveAt(0);
                JostlingArea.RemoveAt(0);
                JostlingArea.RemoveAt(JostlingArea.Count-1);

            }
        }

        public static void ResolveRecurringAbilities()
        {           
            if (JostlingArea.Count(x => x.RecurringAbility == true) > 0)
            {
                List<Animal> repeatAnimals = new List<Animal>();
                for(int i = 0; i < JostlingArea.Count; i++)
                {
                    if (JostlingArea[i].RecurringAbility)
                    {
                        if(!((JostlingArea[i].GetType() == LastPlayedCard.First().Key) && (JostlingArea[i].Colour == LastPlayedCard.First().Value)))
                        {
                            repeatAnimals.Add(JostlingArea[i]);
                        }
                    }                    
                }

                foreach(Animal a in repeatAnimals)
                {
                    a.ActivateAbility();
                }

                repeatAnimals.Clear();                        
            }

            string chameleonExt = " - (Chameleon)";
            bool removeChameleonCopy = JostlingArea.Exists(x => x.Name.Contains(chameleonExt));

            //if last played card was a chameleon in disguise get rid of it and insert the real chameleon.
            if (removeChameleonCopy)
            {
                int chameleonIndex = JostlingArea.FindIndex(x => x.Name.Contains(chameleonExt));
                string animalColour = JostlingArea[chameleonIndex].Colour;
                JostlingArea.RemoveAt(chameleonIndex);
                Chameleon chameleon = new Chameleon(animalColour);
                JostlingArea.Insert(chameleonIndex, chameleon);
            }
        }

        public static string GetResult()
        {

            string outcome;

            //Player with the most animals in bar wins

            Dictionary<string, int> TotalCards = new Dictionary<string, int>();
         
            foreach (Animal a in Entered)
            {            
                if (!(TotalCards.ContainsKey(a.Colour)))
                {
                    TotalCards.Add(a.Colour, 1);
                }
                else
                {
                    TotalCards[a.Colour] += 1; 
                }             
            }

            int MostCardsCount = TotalCards.Values.Max();
            int highestValueKeyCount = TotalCards.Values.Count(x => x == MostCardsCount);

            //if there is a tie between players, the players with the lowest total value of their cards win
            if (highestValueKeyCount > 1)
            {
                var potentialWinningColours = TotalCards.Where(x => x.Value == MostCardsCount).Select(x => x.Key).ToList<string>();

                Dictionary<string, int> TotalStrength = new Dictionary<string, int>();
                foreach (Animal a in Entered)
                {
                    if (potentialWinningColours.Contains(a.Colour))
                    {
                        if (!(TotalStrength.ContainsKey(a.Colour)))
                        {
                            TotalStrength.Add(a.Colour, a.Strength);
                            //TotalStrength.Add(a.Colour, 1); //to test draws
                        }
                        else
                        {
                            TotalStrength[a.Colour] += a.Strength;
                            //TotalStrength[a.Colour] += 1; //to test draws
                        }
                    }  
                }

                int lowestValue = TotalStrength.Values.Min();
                int lowestValueKeyCount = TotalStrength.Values.Count(x => x == lowestValue);

                if(lowestValueKeyCount > 1)
                {
                    var winningColours = TotalStrength.Where(x => x.Value == MostCardsCount).Select(x => x.Key).ToList<string>();
                    outcome = "Draw between: ";

                    for(int j=0; j < winningColours.Count; j++)
                    {                        
                        outcome += Players.Where(x => x.Colour == winningColours[j]).Select(x => x.Name).FirstOrDefault();
                        if(j != winningColours.Count - 1)
                        {
                            outcome += " and ";
                        }
                    }

                    outcome += $" with {MostCardsCount} of your animals making it into the bar with an equal Value of {lowestValue}";
                }
                else
                {
                    string winningColour = TotalStrength.FirstOrDefault(x => x.Value == lowestValue).Key;
                    Player winner = Players.FirstOrDefault(x => x.Colour == winningColour);
                    winner.TotalEntered = MostCardsCount;
                    winner.TotalCardValueEntered = lowestValue;
                    outcome = $"Congratulations {winner.Name}! You won with {MostCardsCount} of your animals making it into the bar with the lowest Value of {lowestValue} for a cheeky bev";
                }


            }
            else
            {
                string winningColour = TotalCards.FirstOrDefault(x => x.Value == MostCardsCount).Key;
                Player winner = Players.FirstOrDefault(x => x.Colour == winningColour);
                winner.TotalEntered = MostCardsCount;
                outcome = $"Congratulations {winner.Name}! You won with {MostCardsCount} of your animals making it into the bar for a cheeky bev";
            }

            

            return outcome;

        }
        public static void ViewEntered()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine();
            sb.Append("[Entered]");
            sb.AppendLine();

            if (Entered.Count > 0)
            {
                Entered = Entered.OrderBy(x => x.Colour).ToList();
                foreach (Animal a in Entered)
                {
                    sb.Append($"  [{a.Name}, {a.Colour}]  ");
                    sb.AppendLine();
                }
            }

            Console.WriteLine(sb.ToString());
        }


        public static void AddTestData()
        {
            foreach(Player p in Players)
            {
                int num = 2;

                if (p.Colour == "Blue" || p.Colour == "Yellow")
                {
                    num = 4;
                }

                for(int i = 0; i < p.Deck.Count - num; i++)
                {
                    Entered.Add(p.Deck.Pop());
                }
            }
        }

    }
}
