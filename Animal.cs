﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeastyBar
{
    public class Animal
    {
        public string Name { get; set; }
        public int Strength { get; set; }
        public string Colour { get; set; }
        public string AbilityInfo { get; set; }
        public bool RecurringAbility { get; set; }

        public Animal()
        {        
            
        }
        public override string ToString()
        {
            return "Name: " + Name + Environment.NewLine +  
                   "Strength: " + Strength.ToString() + Environment.NewLine +               
                   "Ability: " +  AbilityInfo;

        }

        public virtual void ActivateAbility()
        {
            Console.WriteLine($"Activating {Colour} {Name}'s ability...");
            Console.WriteLine(Environment.NewLine);
        }

        protected static int GetCardJostlingNumber()
        {
            string askForCard;
            string confirmCard;
            int cardNum = -1;
            string userConfirmation;
            bool finished = false;
            bool redo = true;
            string userInput;

            while (!finished)
            {

                if (redo)
                {
                    for (int j = 0; j < Setup.JostlingArea.Count; j++)
                    {
                        Console.WriteLine($" [{j + 1}] = [{Setup.JostlingArea[j].Name}, {Setup.JostlingArea[j].Colour}]");
                    }

                    askForCard = $"Enter Number of Card you'd like to select: ";
                    Console.WriteLine(askForCard);
                    userInput = Console.ReadLine();
                    if (!int.TryParse(userInput, out cardNum))
                    {
                        redo = true;
                        Console.WriteLine("Input not recognised, please try again");
                        Console.WriteLine(Environment.NewLine);
                    }
                    else
                    {
                        if (cardNum - 1 >= Setup.JostlingArea.Count || cardNum - 1 < 0)
                        {
                            Console.WriteLine("Input outside of range, please try again");
                            Console.WriteLine(Environment.NewLine);
                            redo = true;
                        }
                        else
                        {
                            redo = false;
                        }

                    }
                }
                else
                {
                    Console.WriteLine(Environment.NewLine);
                    confirmCard = $"Confirm Choice: {cardNum} click y to confirm n to change your mind [y/n]: ";
                    Console.WriteLine(confirmCard);
                    userConfirmation = Console.ReadLine();
                    Console.WriteLine(Environment.NewLine);

                    if (userConfirmation.ToLower() == "y")
                    {
                        finished = true;
                    }
                    else if (userConfirmation.ToLower() == "n")
                    {
                        redo = true;
                    }
                    else
                    {
                        Console.WriteLine("Input not recognised, please try again");
                        Console.WriteLine(Environment.NewLine);
                        redo = false;
                    }
                }
            }

            return cardNum - 1; //because we show hand num as 1 indexed
        }

        protected static int GetJostlingAreaInsertIndex(int startIndex, int endIndex, int strength)
        {
            
            int insertIndex = Setup.JostlingArea.Count;
            
            //go from back to front
            for (int j = startIndex; j > endIndex; j--)
            {
                insertIndex = j;
                if (Setup.JostlingArea[j].Strength >= strength)
                {
                    insertIndex += 1;                    
                    break;
                }
            }  

            return insertIndex;
        }

        protected static int GetLastMatchingAnimalIndex(Type type, string colour = "", bool hasDuplicates = true)
        {
            //when colour is involved you're looking for your animal's current location
            int chosenAnimalIndex = -1;

            for (int i = 0; i < Setup.JostlingArea.Count; i++)
            {
                if (Setup.JostlingArea[i].GetType() == type) 
                {
                    if (colour != "")
                    {
                        if (Setup.JostlingArea[i].Colour == colour)
                        {
                            chosenAnimalIndex = i;

                            if (!hasDuplicates)
                            {
                                break;
                            }                                                     
                        }
                    }
                    else
                    {                     
                        chosenAnimalIndex = i;
                    }
                }
            }          

            return chosenAnimalIndex;
        }

    }


    public class Lion : Animal
    {

        public Lion()
        {
            this.Name = "Lion";
            this.Strength = 12;
            this.AbilityInfo = "Pushes in front of all other Animals towards the gate. " +
                                "Moves all monkey's in the line to the 'That's It' pile. " +
                                "If there's already a Lion in the line, move your Lion to the 'That's It' pile";
            this.RecurringAbility = false;
        }
        public Lion(string colour) : this()
        {           
            this.Colour = colour;
        }

        public Lion(string colour, string nameExt) : this()
        {
            this.Colour = colour;
            this.Name = Name + nameExt;
        }

        public override void ActivateAbility()
        {
            base.ActivateAbility();
            
            if (Setup.JostlingArea.Count > 0)
            {
                if (Setup.JostlingArea.OfType<Lion>().Any())
                {
                    Setup.ThatsIt.Add(this);
                }
                else
                {
                    for (int i = Setup.JostlingArea.Count - 1; i > -1; i--)
                    {
                        if (Setup.JostlingArea[i] is Monkey)
                        {
                            Animal rejectCard = Setup.JostlingArea[i];
                            Setup.ThatsIt.Add(rejectCard);
                            Setup.JostlingArea.RemoveAt(i);
                        }
                    }
                    Setup.JostlingArea.Insert(0, this);
                }
            }
            else
            {
                Setup.JostlingArea.Add(this);
            }                 
        }
    }

    public class Hippo : Animal
    {
        public Hippo()
        {
            this.Name = "Hippo";
            this.Strength = 11;
            this.AbilityInfo = "Pushes in front of weaker animals except for Zebra's and other Hippo's. (Repeats every turn).";
            this.RecurringAbility = true;
        }
        public Hippo(string colour) : this()
        {
            this.Colour = colour;            
        }

        public Hippo(string colour, string nameExt) : this()
        {
            this.Colour = colour;
            this.Name = Name + nameExt;            
        }
        public override void ActivateAbility()
        {
            base.ActivateAbility();

            if (Setup.JostlingArea.Count == 0)
            {
                //For empty list
                Setup.JostlingArea.Add(this);
            }
            else
            {
                int insertHippoIndex;
                
                int lastZebraIndex = GetLastMatchingAnimalIndex(typeof(Zebra));                
                int yourHippoIndex = GetLastMatchingAnimalIndex(typeof(Hippo), this.Colour);

                //not yet in list
                if (yourHippoIndex == -1)
                {
                    //zebra in line
                    if (lastZebraIndex != -1)
                    {
                        insertHippoIndex = GetJostlingAreaInsertIndex(Setup.JostlingArea.Count - 1, lastZebraIndex, this.Strength);
                    }
                    else
                    {
                        insertHippoIndex = GetJostlingAreaInsertIndex(Setup.JostlingArea.Count - 1 , - 1, this.Strength);
                    }

                    Setup.JostlingArea.Insert(insertHippoIndex, this);

                }
                else
                {
                    if (lastZebraIndex != -1)
                    {
                        insertHippoIndex = GetJostlingAreaInsertIndex(yourHippoIndex - 1, lastZebraIndex, this.Strength);
                    }
                    else
                    {
                        if(yourHippoIndex > 0)
                        {
                            insertHippoIndex = GetJostlingAreaInsertIndex(yourHippoIndex - 1, -1, this.Strength);
                        }
                        else
                        {
                            insertHippoIndex = 0;
                        }
                        
                    }

                    if (insertHippoIndex < yourHippoIndex)
                    {
                        Setup.JostlingArea.Insert(insertHippoIndex, this);

                        yourHippoIndex = GetLastMatchingAnimalIndex(typeof(Hippo), this.Colour);

                        //you've added your hippo to its new spot so you can now get rid of where it used to be
                        if (yourHippoIndex != -1)
                        {
                            Setup.JostlingArea.RemoveAt(yourHippoIndex);
                        }
                    }
                }
            }
        }
    }

    public class Crocodile : Animal
    {
        public Crocodile()
        {
            this.Name = "Crocodile";
            this.Strength = 10;
            this.AbilityInfo = "Eats all weaker animals in front of it. " +
                                "If it encounters a stronger animal or zebra while eating, the crocodile immediately stops. " +
                                "Eaten animals are discarded on the THAT'S IT card. (Repeats every turn).";
            this.RecurringAbility = true;
        }
        public Crocodile(string colour) : this()
        {
            this.Colour = colour;            
        }

        public Crocodile(string colour, string nameExt) : this()
        {
            this.Colour = colour;
            this.Name = Name + nameExt;          
        }
        public override void ActivateAbility()
        {
            base.ActivateAbility();

            if (Setup.JostlingArea.Count == 0)
            {                
                Setup.JostlingArea.Add(this);
            }
            else
            {
                int insertCrocIndex;
                int start;
                int end;
                int lastZebraIndex = GetLastMatchingAnimalIndex(typeof(Zebra));
                int yourCrocIndex = GetLastMatchingAnimalIndex(typeof(Crocodile), this.Colour);
                
                //Not yet in list
                if(yourCrocIndex == -1)
                {                    
                    if (lastZebraIndex != -1)
                    {
                        insertCrocIndex = GetJostlingAreaInsertIndex(Setup.JostlingArea.Count - 1, lastZebraIndex, this.Strength);
                    }
                    else
                    {
                        insertCrocIndex = GetJostlingAreaInsertIndex(Setup.JostlingArea.Count - 1, - 1, this.Strength);                        
                    }

                    //if just added we need to eat everything after us or the zebra 

                    Setup.JostlingArea.Insert(insertCrocIndex, this);
                    start = Setup.JostlingArea.Count - 1;
                    end = insertCrocIndex;                                      
                }
                else
                {

                    //if repeating we need to eat everything before us unless zebra 
                    start = yourCrocIndex;
                    
                    if (lastZebraIndex < start)
                    {
                        end = lastZebraIndex;
                    }
                    else
                    {
                        end = -1;
                    }
                }                

                for (int i = start; i > end; i--)
                {
                    if (Setup.JostlingArea[i].Strength < this.Strength)
                    {
                        Animal rejectCard = Setup.JostlingArea[i];
                        Setup.ThatsIt.Add(rejectCard);
                        Setup.JostlingArea.RemoveAt(i);
                    }
                }                                         
            }
        }
    }

    public class Snake : Animal
    {
        public Snake()
        {
            this.Name = "Snake";
            this.Strength = 9;
            this.AbilityInfo = "Re-Orders the line in order of strength. Members of the same species don't change the order among them.";
            this.RecurringAbility = false;
        }
        public Snake(string colour) : this()
        {
            this.Colour = colour;           
        }

        public Snake(string colour, string nameExt) : this()
        {
            this.Colour = colour;
            this.Name = Name + nameExt;            
        }
        public override void ActivateAbility()
        {
            base.ActivateAbility();
            Setup.JostlingArea.Add(this);            
            Setup.JostlingArea = Setup.JostlingArea.OrderByDescending(x => x.Strength).ToList(); //Beta test to check animals of same species retain order 
        }
    }

    public class Giraffe : Animal
    {
        public Giraffe()
        {
            this.Name = "Giraffe";
            this.Strength = 8;
            this.AbilityInfo = "Passes one weaker animal that stands directly in front, If there is no weaker animal, it stays put. (Repeats every turn). ";
            this.RecurringAbility = true;
        }
        public Giraffe(string colour) : this()
        {
            this.Colour = colour;           
        }

        public Giraffe(string colour, string nameExt) : this()
        {
            this.Colour = colour;
            this.Name = Name + nameExt;
        }
        public override void ActivateAbility()
        {
            base.ActivateAbility();
            int yourGiraffeIndex = GetLastMatchingAnimalIndex(typeof(Giraffe), this.Colour, false);

            //if it was -1 it wasn't in the list
            if(yourGiraffeIndex == -1)
            {
                Setup.JostlingArea.Add(this);
                yourGiraffeIndex = Setup.JostlingArea.Count - 1; //just added to the end
            }

            //if its 0 its at the start
            if (yourGiraffeIndex > 0)
            {
                if(this.Strength > Setup.JostlingArea[yourGiraffeIndex-1].Strength)
                {
                    Animal weakerAnimal = Setup.JostlingArea[yourGiraffeIndex - 1];
                    Setup.JostlingArea[yourGiraffeIndex - 1] = Setup.JostlingArea[yourGiraffeIndex]; //your giraffe
                    Setup.JostlingArea[yourGiraffeIndex] = weakerAnimal;
                }     
            }
        }
    }

    public class Zebra : Animal
    {
        public Zebra()
        {
            this.Name = "Zebra";
            this.Strength = 7;
            this.AbilityInfo = "Stops Hippo's and Crocodiles from moving in front (Repeats every turn)";
            this.RecurringAbility = false; //Technically is recurring but will just be baked into the logic for hippo's and crocs
        }
        public Zebra(string colour) : this()
        {
            this.Colour = colour;           
        }
        public Zebra(string colour, string nameExt) : this()
        {
            this.Colour = colour;
            this.Name = Name + nameExt;
        }
        public override void ActivateAbility()
        {
            base.ActivateAbility();
            Setup.JostlingArea.Add(this);
        }
    }

    public class Seal : Animal
    {
        public Seal()
        {
            this.Name = "Seal";
            this.Strength = 6;
            this.AbilityInfo = "Swaps the position of the 'Gate' and 'That's It' cards around";
            this.RecurringAbility = false;
        }
        public Seal(string colour) : this()
        {
            this.Colour = colour;            
        }
        public Seal(string colour, string nameExt) : this()
        {
            this.Colour = colour;
            this.Name = Name + nameExt;
        }
        public override void ActivateAbility()
        {
            base.ActivateAbility();
            Setup.JostlingArea.Add(this);
            Setup.JostlingArea.Reverse();
            Setup.EntranceLeft = !Setup.EntranceLeft;
            
        }
    }

    public class Chameleon : Animal
    {
        public Chameleon(string colour)
        {
            this.Colour = colour;
            this.Name = "Chameleon";
            this.Strength = 5;
            this.AbilityInfo = "Carries out the action of a species that is present in the Jostling area. For this action (only), the chameleon also takes on the strength of the imitated species." +
                "as soon as the 'recurring' animal actions are carried out (even on the same turn), the chameleon goes back to its original strength.";
            this.RecurringAbility = false;
        }
        public override void ActivateAbility()
        {
            base.ActivateAbility();

            if (Setup.JostlingArea.Count == 0)
            {
                Setup.JostlingArea.Add(this);
            }
            else
            {
                int cardToCopy = GetCardJostlingNumber();
                string cardCopyName = Setup.JostlingArea[cardToCopy].Name.ToLower();
                string chameleonNameExt = " - (Chameleon)";

                Animal animal = new Animal();

                if (cardCopyName == "chameleon")
                {
                    Setup.JostlingArea.Add(this);
                }
                else
                {
                    switch (cardCopyName)
                    {
                        case "skunk":
                            animal = new Skunk(this.Colour, chameleonNameExt);
                            break;
                        case "parrot":
                            animal = new Parrot(this.Colour, chameleonNameExt);
                            break;
                        case "kangaroo":
                            animal = new Kangaroo(this.Colour, chameleonNameExt);
                            break;
                        case "monkey":
                            animal = new Monkey(this.Colour, chameleonNameExt);
                            break;
                        case "seal":
                            animal = new Seal(this.Colour, chameleonNameExt);
                            break;
                        case "zebra":
                            animal = new Zebra(this.Colour, chameleonNameExt);
                            break;
                        case "giraffe":
                            animal = new Giraffe(this.Colour, chameleonNameExt);
                            break;
                        case "snake":
                            animal = new Snake(this.Colour, chameleonNameExt);
                            break;
                        case "crocodile":
                            animal = new Crocodile(this.Colour, chameleonNameExt);
                            break;
                        case "hippo":
                            animal = new Hippo(this.Colour, chameleonNameExt);
                            break;
                        case "lion":
                            animal = new Lion(this.Colour, chameleonNameExt);
                            break;
                    }

                    animal.ActivateAbility();
                    
                    //get rid of it afterwards and replace with the chameleon
                }
            }                           
        }
    }

    public class Monkey : Animal
    {
        public Monkey()
        {
            this.Name = "Monkey";
            this.Strength = 4;
            this.AbilityInfo = "If another monkey is in the Jostling area, the monkeys shoo all waiting Hippos and Crocodiles onto the THAT'S IT card." +
                "Then the just played monkey pushes its way past all the other animals to Heaven's Gate, gathering its monkey mates that have been waiting in line directly behind it - now in reverse order.";
            this.RecurringAbility = false;
        }
        public Monkey(string colour) : this()
        {
            this.Colour = colour;            
        }
        public Monkey(string colour, string nameExt) : this()
        {
            this.Colour = colour;
            this.Name = Name + nameExt;
        }
        public override void ActivateAbility()
        {
            base.ActivateAbility();
            if (Setup.JostlingArea.Count == 0 || Setup.JostlingArea.Count(x => x is Monkey) < 1)
            {
                Setup.JostlingArea.Add(this);
            }
            else
            {
                List<Monkey> monkeys = new List<Monkey>();
                //there are other monkeys
                for (int i = Setup.JostlingArea.Count - 1; i > -1; i--)
                {
                    if (Setup.JostlingArea[i] is Hippo || Setup.JostlingArea[i] is Crocodile)
                    {
                        Animal rejectCard = Setup.JostlingArea[i];
                        Setup.ThatsIt.Add(rejectCard);
                        Setup.JostlingArea.RemoveAt(i);
                    }
                    else if(Setup.JostlingArea[i] is Monkey)
                    {
                        Monkey monkey = (Monkey)Setup.JostlingArea[i];  
                        monkeys.Add(monkey);
                        Setup.JostlingArea.RemoveAt(i);
                    }
                }

                for(int j = monkeys.Count - 1; j > -1; j--)
                {
                    Setup.JostlingArea.Insert(0, monkeys[j]);
                }
                
                monkeys.Clear();
                Setup.JostlingArea.Insert(0, this);               
            }
            
        }
    }

    public class Kangaroo : Animal
    {
        public Kangaroo()
        {
            this.Name = "Kangaroo";
            this.Strength = 3;
            this.AbilityInfo = "Jumps over the last or last two animals in the line. The strength of the jumped-over animals doesn't play a role in this.";
            this.RecurringAbility = false;
        }
        public Kangaroo(string colour) : this()
        {
            this.Colour = colour;            
        }
        public Kangaroo(string colour, string nameExt) : this()
        {
            this.Colour = colour;
            this.Name = Name + nameExt;
        }
        public override void ActivateAbility()
        {
            base.ActivateAbility();

            string askForCard;
            string confirmCard;
            int jumpSpaces = -1;
            string userConfirmation;
            bool finished = false;
            bool redo = true;
            string userInput;
            int JostlingAreaCount = Setup.JostlingArea.Count;
            int end;

            if (JostlingAreaCount > 1)
            {
                //TODO: generecise this
                while (!finished)
                {

                    if (redo)
                    {
                        // Type your username and press enter
                        askForCard = $"How many cards will your Kangaroo jump over? [1] or [2]";
                        Console.WriteLine(askForCard);
                        userInput = Console.ReadLine();
                        if (!int.TryParse(userInput, out jumpSpaces))
                        {
                            redo = true;
                            Console.WriteLine("Input not recognised, please try again");
                            Console.WriteLine(Environment.NewLine);
                        }
                        else
                        {
                            if (jumpSpaces > 2 || jumpSpaces < 1)
                            {
                                Console.WriteLine("Input outside of range, please try again");
                                Console.WriteLine(Environment.NewLine);
                                redo = true;
                            }
                            else
                            {
                                redo = false;
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine(Environment.NewLine);
                        confirmCard = $"Confirm Choice: {jumpSpaces} click y to confirm n to change your mind [y/n]: ";
                        Console.WriteLine(confirmCard);
                        userConfirmation = Console.ReadLine();
                        Console.WriteLine(Environment.NewLine);

                        if (userConfirmation.ToLower() == "y")
                        {
                            finished = true;
                        }
                        else if (userConfirmation.ToLower() == "n")
                        {
                            redo = true;
                        }
                        else
                        {
                            Console.WriteLine("Input not recognised, please try again");
                            Console.WriteLine(Environment.NewLine);
                            redo = false;
                        }
                    }
                }

                end = JostlingAreaCount;
                Setup.JostlingArea.Insert(end - jumpSpaces, this);

            }
            else if (JostlingAreaCount == 1)
            {
                //always jumps over the last one if there's only one
                Setup.JostlingArea.Insert(JostlingAreaCount - 1, this);
            }
            else
            {
                Setup.JostlingArea.Add(this);
            }
        }
    }

    public class Parrot : Animal
    {
        public Parrot()
        {
            this.Name = "Parrot";
            this.Strength = 2;
            this.AbilityInfo = "Shoos an animal of your choice out of the Jostling area onto the THAT'S IT card.";
            this.RecurringAbility = false;
        }
        public Parrot(string colour) : this()
        {
            this.Colour = colour;           
        }
        public Parrot(string colour, string nameExt) : this()
        {
            this.Colour = colour;
            this.Name = Name + nameExt;
        }
        public override void ActivateAbility()
        {
            base.ActivateAbility();
            if (Setup.JostlingArea.Count > 0)
            {
                int cardToRemove = GetCardJostlingNumber();
                Animal card = Setup.JostlingArea[cardToRemove];

                //Add to line
                Setup.ThatsIt.Add(card);

                //Remove card from hand
                Setup.JostlingArea.RemoveAt(cardToRemove);
            }

            Setup.JostlingArea.Add(this);

        }

    }

    public class Skunk : Animal
    {
        public Skunk()
        {
            this.Name = "Skunk";
            this.Strength = 1;
            this.AbilityInfo = "Expels the two strongest currently displayed species - but never other skunks. The expelled animals are put on the THAT'S IT card.";
            this.RecurringAbility = false;
        }
        public Skunk(string colour) : this()
        {
            this.Colour = colour;          
        }
        public Skunk(string colour, string nameExt) : this()
        {
            this.Colour = colour;
            this.Name = Name + nameExt;
        }

        public override void ActivateAbility()
        {
            base.ActivateAbility();
            //get highest strength

            Setup.JostlingArea.Add(this);
            
            for (int i = 0; i < 2; i++)
            {
                if (Setup.JostlingArea.Count > 0)
                {
                    var highestStrengthVal = Setup.JostlingArea.Max(x => x.Strength);
                    if (highestStrengthVal > 1)
                    {
                        for(int j = Setup.JostlingArea.Count - 1; j > -1; j--)
                        {
                            if(Setup.JostlingArea[j].Strength == highestStrengthVal)
                            {
                                Setup.JostlingArea.RemoveAt(j);
                            }
                        }
                    }
                }
            }
        }
    }
}
