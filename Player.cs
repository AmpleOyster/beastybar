﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace BeastyBar
{
    public class Player
    {
        public List<Animal> Hand { get; set; }
        public Stack<Animal> Deck { get; set; }
        public string Name { get; set; }
        public int ID { get; set; }
        public string Colour { get; set; }
        public int TotalEntered { get; set; }
        public int TotalCardValueEntered { get; set; }


        public Player(string name, int id, string colour)
        {
            Name = name;
            ID = id;
            Colour = colour;
            Deck = new Stack<Animal>();
            Hand = new List<Animal>();            
        }

        public void DealCards()
        {
            //create our animals then add to deck
            Random rnd = new Random();
            List<int> cardPos = Enumerable.Range(1, 12).OrderBy(x => rnd.Next()).ToList();

            Animal animal = new Animal();


            for (int i = 0; i < cardPos.Count; i++)
            {
                switch (cardPos[i])
                {
                    case 1:
                        animal = new Skunk(this.Colour);
                        break;
                    case 2:
                        animal = new Parrot(this.Colour);
                        break;
                    case 3:
                        animal = new Kangaroo(this.Colour);
                        break;
                    case 4:
                        animal = new Monkey(this.Colour);
                        break;
                    case 5:
                        animal = new Chameleon(this.Colour);
                        break;
                    case 6:
                        animal = new Seal(this.Colour);
                        break;
                    case 7:
                        animal = new Zebra(this.Colour);
                        break;
                    case 8:
                        animal = new Giraffe(this.Colour);
                        break;
                    case 9:
                        animal = new Snake(this.Colour);
                        break;
                    case 10:
                        animal = new Crocodile(this.Colour);
                        break;
                    case 11:
                        animal = new Hippo(this.Colour);
                        break;
                    case 12:
                        animal = new Lion(this.Colour);
                        break;
                }

                Deck.Push(animal);                
                
                //debug
                //Console.WriteLine(cardPos[i].ToString());
                //Console.WriteLine(animal.ToString());
            }            
        }

        public void ViewHand()
        {
            for(int i = 0; i < Hand.Count; i++)
            {
                Console.WriteLine(Environment.NewLine);
                //hand number show starting from 1
                Console.WriteLine($"Hand Num: {i + 1}"); 
                Console.WriteLine(Hand[i].ToString());
                
            }
        }

        public void Draw(int numOfCards)
        {
            if (numOfCards <= Deck.Count())
            {
                for (int i = 0; i < numOfCards; i++)
                {
                    Animal animal = Deck.Pop();
                    Hand.Add(animal);
                }
            }
        }

        public void ViewField()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine();

            //TODO: Cleanup Logic
            if (Setup.EntranceLeft)
            {
                sb.Append("[Gate]  ");

                if (Setup.JostlingArea.Count > 0)
                {
                    foreach (Animal a in Setup.JostlingArea)
                    {
                        sb.Append($"  [{a.Name}, {a.Strength}, {a.Colour}]  ");
                    }
                }

                int spaces = 5 - Setup.JostlingArea.Count;
                if (spaces > 0)
                {
                    for (int i = 0; i < spaces; i++)
                    {
                        sb.Append("  [ ]  ");
                    }
                }

                sb.Append("  [That's It]");

            }
            else
            {
                sb.Append("[That's It]  ");
                
                int spaces = 5 - Setup.JostlingArea.Count;
                if (spaces > 0)
                {
                    for (int i = 0; i < spaces; i++)
                    {
                        sb.Append("  [ ]  ");
                    }
                }

                for(int j = Setup.JostlingArea.Count - 1; j > -1; j--)
                {
                    sb.Append($"  [{Setup.JostlingArea[j].Name}, {Setup.JostlingArea[j].Strength}, {Setup.JostlingArea[j].Colour}]  ");
                }

                sb.Append("  [Gate]");
            }
                                                           
            sb.AppendLine();
            Console.WriteLine(sb.ToString());
        }

        public void PlayCard()
        {
            int cardHandNum = GetCardHandNumber();

            //maybe add to the line when activating ability
            Hand[cardHandNum].ActivateAbility();

            Setup.LastPlayedCard.Clear();
            Setup.LastPlayedCard.Add(Hand[cardHandNum].GetType(), Hand[cardHandNum].Colour);           

            //Remove card from hand
            Hand.RemoveAt(cardHandNum);           

        }

        private int GetCardHandNumber()
        {
            string askForCard;
            string confirmCard;
            int cardNum = -1;
            string userConfirmation;
            bool finished = false;
            bool redo = true;
            string userInput;

            while (!finished)
            {

                if (redo)
                {
                    // Type your username and press enter
                    askForCard = $"Enter Hand Number of Card you'd like to Play: ";
                    Console.WriteLine(askForCard);
                    userInput = Console.ReadLine();
                    if (!int.TryParse(userInput, out cardNum))
                    {
                        redo = true;
                        Console.WriteLine("Input not recognised, please try again");
                        Console.WriteLine(Environment.NewLine);
                    }
                    else
                    {
                        if (cardNum - 1 >= Hand.Count || cardNum - 1 < 0)
                        {
                            Console.WriteLine("Input outside of range, please try again");
                            Console.WriteLine(Environment.NewLine);
                            redo = true;
                        }
                        else
                        {
                            redo = false;
                        }                            
                    }
                }
                else
                {
                    Console.WriteLine(Environment.NewLine);
                    confirmCard = $"Confirm Choice: {cardNum} click y to confirm n to change your mind [y/n]: ";
                    Console.WriteLine(confirmCard);
                    userConfirmation = Console.ReadLine();
                    Console.WriteLine(Environment.NewLine);

                    if (userConfirmation.ToLower() == "y")
                    {
                        finished = true;
                    }
                    else if (userConfirmation.ToLower() == "n")
                    {
                        redo = true;
                    }
                    else
                    {
                        Console.WriteLine("Input not recognised, please try again");
                        Console.WriteLine(Environment.NewLine);
                        redo = false;
                    }
                }
            }

            return cardNum - 1; //because we show hand num as 1 indexed
        }

       


    }




}
