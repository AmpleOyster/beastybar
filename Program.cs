﻿using System;

namespace BeastyBar
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("-----------------------------------------------------------------------------------");
            Console.WriteLine("------------------------- WELCOME TO THE BEASTY BAR -------------------------------");
            Console.WriteLine("-----------------------------------------------------------------------------------");

            Console.WriteLine(Environment.NewLine);
            Console.WriteLine("PRESS ANY KEY TO START....");
            Console.WriteLine(Environment.NewLine);

            Console.ReadKey();

            Setup.AddPlayers();

            //debugging features
            //Setup.AddTestData();
            //Console.WriteLine(Setup.GetResult());
            //Console.ReadKey();

            bool done = false;
            int roundCount = 0;

            while (!done)
            {
                foreach (Player p in Setup.Players)
                {                   
                    if (roundCount < 1)
                    {
                        p.Draw(4);
                    }
                    
                    if (p.Hand.Count > 0)
                    {
                        Console.WriteLine($"[Round: {roundCount + 1}] --- {p.Name}'s turn --- [Colour: {p.Colour}] --- [No. cards left in Deck: {p.Deck.Count}]");
                        p.ViewHand();
                        p.ViewField();

                        p.PlayCard();

                        Console.WriteLine("------------------------After Card Played-----------------------");
                        Console.WriteLine(Environment.NewLine);
                        p.ViewField();
                        Console.WriteLine(Environment.NewLine);

                        Setup.ResolveRecurringAbilities();

                        Console.WriteLine("------------------------After Recurring Abilities Resolved-----------------------");
                        Console.WriteLine(Environment.NewLine);
                        p.ViewField();
                        Console.WriteLine(Environment.NewLine);

                        Setup.ResolveRoundEnd();

                        Console.WriteLine("------------------------After Round Resolved-----------------------");
                        Console.WriteLine(Environment.NewLine);
                        p.ViewField();
                        Console.WriteLine(Environment.NewLine);

                        if (p.Deck.Count > 0)
                        {
                            p.Draw(1);
                        }

                    }
                    else
                    {
                        done = true;
                    }   
                    
                    Console.ReadKey();
                }

                roundCount++;
            }

            string result = Setup.GetResult();
            Setup.ViewEntered();
            Console.WriteLine(result);

            Console.ReadKey();

            Console.WriteLine("------------------------Finished-----------------------");
            Console.WriteLine(Environment.NewLine);

            Console.ReadKey();

        }
    }
}
